import * as _ from 'lodash';
import * as Request from 'request-promise'

import { config } from '../../config'

export interface CoreRequestOptions {
    fields?: any
    filter?: any
    limit?: number
    offset?: number
    page?: number
    order?: any
    [key: string]: any
}


export class BaseCoreService {
    constructor(path: string) {
        this.path = path
    }
    path: string
    baseUrl(subpath: string = "") {
        return `${config.server.serverHost}/api/v1/${this.path}/${subpath}`
    }
    protected _paserQuery(query: CoreRequestOptions = {}) {
        const parsedQuery = _.merge({}, query)
        if (query.filter) {
            parsedQuery.filter = (JSON.stringify(query.filter));
        }
        if (query.order) {
            parsedQuery.order = (JSON.stringify(query.order));
        }
        if (query.scopes) {
            parsedQuery.scopes = (JSON.stringify(query.scopes));
        }
        if (query.fields) {
            parsedQuery.fields = (JSON.stringify(query.fields));
        }
        if (query.items) {
            parsedQuery.items = (JSON.stringify(query.items));
        }
        return parsedQuery;
    }
    async exec(promise: Request.RequestPromise) {
        try {
            return await promise
        } catch (error) {
            //throw error
            console.log("err:", error)
        }
    }
    async find(query: CoreRequestOptions = {}) {
        const options: Request.Options = {
            uri: this.baseUrl("find"),
            method: "GET",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'client_id': config.app.client_id,
                'client_secret': config.app.client_secret
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object
        } else {
            return {}
        }
    }
    async getItem(itemId: string, query: CoreRequestOptions = {}) {
        const options: Request.Options = {
            uri: this.baseUrl(itemId),
            method: "GET",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'client_id': config.app.client_id,
                'client_secret': config.app.client_secret
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object
        } else {
            return {}
        }
    }
    async getItemBySlug(slug: string, query: CoreRequestOptions = {}) {
        query.filter ? query.filter.slug = slug : query.filter = { slug: slug }
        const options: Request.Options = {
            uri: this.baseUrl("find"),
            method: "GET",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'client_id': config.app.client_id,
                'client_secret': config.app.client_secret
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object
        } else {
            return {}
        }
    }

    async getList(query: CoreRequestOptions = {}) {
        const options: Request.Options = {
            uri: this.baseUrl(),
            method: "GET",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'client_id': config.app.client_id,
                'client_secret': config.app.client_secret
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.results && res.results.objects) {
            return {
                ...res.results.objects, pagination: res.pagination
            }
        } else {
            return {
                rows: [],
                count: 0,
                pagination: {}
            }
        }
    }
    async delete(itemId: string, query: CoreRequestOptions = {}) {
        const options: Request.Options = {
            uri: this.baseUrl(itemId),
            method: "DELETE",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'client_id': config.app.client_id,
                'client_secret': config.app.client_secret
            },
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object
        } else {
            return {}
        }
    }
    async update(itemId: string, body: any, query: CoreRequestOptions = {}) {
        const options: Request.Options = {
            uri: this.baseUrl(itemId),
            method: "PUT",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/x-www-form-urlencoded',
                'client_id': config.app.client_id,
                'client_secret': config.app.client_secret
            },
            body: body,
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object
        } else {
            return {}
        }
    }
    async create(body: any, query: CoreRequestOptions = {}) {
        const options: Request.Options = {
            uri: this.baseUrl(),
            method: "POST",
            qs: this._paserQuery(query),
            headers: {
                'User-Agent': 'Request-Promise',
                'Content-Type': 'application/json',
                'client_id': config.app.client_id,
                'client_secret': config.app.client_secret
            },
            body: body,
            json: true // Automatically parses the JSON string in the response
        };
        const res = await this.exec(Request(options));
        if (res && res.result && res.result.object) {
            return res.result.object
        } else {
            return {}
        }
    }

}