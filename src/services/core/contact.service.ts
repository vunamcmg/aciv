import { BaseCoreService } from './base.service'


export class ContactCoreService extends BaseCoreService {
    constructor() {
        super("contact")
    }
}