import { AboutCoreService } from './about.service'
import { AppCoreService } from './app.service'
import { BlogCoreService } from './blog.service'
import { PostCoreService } from './post.service'
import { ProductCoreService } from './product.service'
import { ProjectCoreService } from './project.service'
import { ShopCoreService } from './shop.service'
import { CommentCoreService } from './comment.service'
import { ContactCoreService } from './contact.service'
import { ServiceCoreService } from './service.service'
import { TeamCoreService } from './team.service'
import { TesmonialCoreService } from './tesmonial.service'
import { PageCoreService } from './page.service'
import { ProjectCategoryCoreService } from './projectCategory.service'


const aboutCoreService = new AboutCoreService()
const appCoreService = new AppCoreService()
const blogCoreService = new BlogCoreService()
const postCoreService = new PostCoreService()
const productCoreService = new ProductCoreService()
const projectCoreService = new ProjectCoreService()
const shopCoreService = new ShopCoreService()
const commentCoreService = new CommentCoreService()
const contactCoreService = new ContactCoreService()
const serviceCoreService = new ServiceCoreService()
const teamCoreService = new TeamCoreService()
const tesmonialCoreService = new TesmonialCoreService()
const pageCoreService = new PageCoreService()
const projectCategoryCoreService = new ProjectCategoryCoreService()

export {
    aboutCoreService,
    appCoreService,
    blogCoreService,
    postCoreService,
    productCoreService,
    projectCoreService,
    shopCoreService,
    commentCoreService,
    contactCoreService,
    serviceCoreService,
    teamCoreService,
    tesmonialCoreService,
    pageCoreService,
    projectCategoryCoreService
}