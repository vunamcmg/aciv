import { BaseCoreService } from './base.service'


export class ShopCoreService extends BaseCoreService {
    constructor() {
        super("shop")
    }
}