import { BaseCoreService } from './base.service'


export class AboutCoreService extends BaseCoreService {
    constructor() {
        super("about")
    }
}