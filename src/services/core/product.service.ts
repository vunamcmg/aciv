import { BaseCoreService } from './base.service'


export class ProductCoreService extends BaseCoreService {
    constructor() {
        super("product")
    }
}