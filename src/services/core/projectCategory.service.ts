import { BaseCoreService } from './base.service'


export class ProjectCategoryCoreService extends BaseCoreService {
    constructor() {
        super("projectCategory")
    }
}