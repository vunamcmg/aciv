import { BaseCoreService } from './base.service'


export class TeamCoreService extends BaseCoreService {
    constructor() {
        super("team")
    }
}