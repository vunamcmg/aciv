import * as dotenv from 'dotenv'
dotenv.config({ silent: true })
export default {
    server: {
        host: 'localhost',
        protocol: 'http',
        debug: true,
        port: process.env.PORT || 3000,
        uiHost: 'http://localhost:3000',
        serverHost: process.env.HOST,
        version: 'v1',
    },
    database: {
        //mongo: process.env.MONGOLOCAL_URI,
        mongo: process.env.MONGOLOCAL_URI,
        defaultPageSize: 50,
    },
    app: {
        _id: process.env.APP_ID,
        token: "",
        client_id: process.env.CLIENT_ID,
        client_secret: process.env.CLIENT_SECRET
    }


}