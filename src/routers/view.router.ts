import { BaseRouter, Request, Response } from "./base.router";
import * as express from 'express'
import * as path from 'path'
import { appCoreService, blogCoreService, contactCoreService, postCoreService, serviceCoreService, teamCoreService, pageCoreService, projectCoreService, projectCategoryCoreService } from "../services";
import { config } from "../config";
import * as MobileDetect from 'mobile-detect'

export class ViewRouter extends BaseRouter {
    constructor() {
        super()
        this.router = express.Router();
        this.routing()
    }
    router: express.Router
    routing() {
        // Trang chủ
        this.router.get("/", this.route(this.renderHome))
        this.router.get("/index", this.route(this.renderHome))

        // Blog
        this.router.get("/bai-viet", this.route(this.renderBlog))
        this.router.get("/blog", this.route(this.renderBlog))
        //this.router.get("/bai-viet/:categorySlug", this.route(this.renderPostCategory))
        this.router.get("/bai-viet/:slug", this.route(this.renderPost))
        this.router.get("/post", this.route(this.renderPost))
        this.router.post("/comment", this.route(this.createComment))

        // Liên hệ
        this.router.get("/lien-he", this.route(this.renderContact))
        this.router.get("/contact", this.route(this.renderContact))
        this.router.post("/lien-he", this.route(this.createContact))
        this.router.post("/contact", this.route(this.createContact))

        // Giới thiệu
        this.router.get("/gioi-thieu", this.route(this.renderAbout))
        this.router.get("/about", this.route(this.renderAbout))

        // Dịch vụ
        this.router.get("/dich-vu", this.route(this.renderServices))
        this.router.get("/services", this.route(this.renderServices))

        this.router.get("/dich-vu/:slug", this.route(this.renderServiceDetail))
        this.router.get("/services/:slug", this.route(this.renderServiceDetail))

        // Dự án
        this.router.get("/du-an", this.route(this.renderProjects))
        this.router.get("/projects", this.route(this.renderProjects))

        this.router.get("/du-an/:slug", this.route(this.renderProjectDetail))
        this.router.get("/project/:slug", this.route(this.renderProjectDetail))

        // Đội ngũ
        this.router.get("/doi-ngu", this.route(this.renderTeam))
        this.router.get("/team", this.route(this.renderTeam))
        this.router.get("/doi-ngu/:slug", this.route(this.renderTeamDetail))
        this.router.get("/team/:slug", this.route(this.renderTeamDetail))

        // Sự kiện
        this.router.get("/su-kien", this.route(this.renderEvents))
        this.router.get("/events", this.route(this.renderEvents))
        this.router.get("/su-kien/:slug", this.route(this.renderEventsDetail))
        this.router.get("/events/:slug", this.route(this.renderEventsDetail))

        // Time line
        this.router.get("/hanh-trinh", this.route(this.renderTimeline))
        this.router.get("/timeline", this.route(this.renderTimeline))

        // Thương mại điện tử
        this.router.get("/cua-hang", this.route(this.renderShop))
        this.router.get("/cua-hang/:categorySlug", this.route(this.renderProductCategory))
        this.router.get("/cua-hang/:categorySlug/:productSlug", this.route(this.renderProduct))
        this.router.get("/gio-hang", this.route(this.renderCart))
        this.router.get("/dat-hang", this.route(this.renderCheckoutProcess))
        this.router.get("/dat-hang-thanh-cong", this.route(this.renderCheckoutSuccess))

        // Tài khoản
        this.router.get("/dang-nhap", this.route(this.renderLogin))
        this.router.get("/dang-ky", this.route(this.renderSignUp))
        this.router.get("/tai-khoan", this.route(this.renderProfile))

        // Lỗi
        this.router.get("/404", this.route(this.renderError))


        //this.router.get(/(^[A-Za-z][^.]*$)|^[A-Za-z]*$/, this.route(this.renderDynamic))
        this.router.get(/^[^api][A-Za-z0-9][^.]*$/, (function (req, res, next) {
            if (req.path.includes("api")) {
                next()
            } else {
                let filePath = req.path[req.path.length - 1] === "/" ? req.path.slice(0, req.path.length - 1) : req.path
                const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
                res.render(path.join(__dirname, `../../views${filePath}`), { setting }, function (error, html) {
                    if (error) {
                        res.redirect("/404")
                    } else {
                        res.send(html)
                    }
                })

            }
        }))
        //this.router.get(/^\/api\/+$/, this.route(this.renderDynamic))
    }
    async renderLogin(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/login`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderSignUp(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/signup`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderProfile(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/profile`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async renderShop(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/shop`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderProductCategory(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/productCategory`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderProduct(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/product`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderCart(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/cart`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderCheckoutProcess(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/checkoutProcess`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderCheckoutSuccess(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/checkoutSuccess`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderDynamic(req: Request, res: Response) {
        try {
            let filePath = req.path[req.path.length - 1] === "/" ? req.path.slice(0, req.path.length - 1) : req.path
            const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
            res.render(path.join(__dirname, `../../views${filePath}`), { setting }, function (error, html) {
                if (error) {
                    res.redirect("/404")
                } else {
                    res.send(html)
                }
            })
        } catch (err) {
            res.redirect("/404")
        }
    }
    async renderHome(req: Request, res: Response) {
        let pageData = {}
        
        let isMobile: boolean = false
        const md = new MobileDetect(req.headers["user-agent"]);
        if (md.os() === "AndroidOS" || md.os() === "iOS") {
            isMobile = true
        }
        const projectData = await projectCoreService.getList({
            filter: {
                status: "active",
                isPin: true
            },
            fields: ["$all"],
            limit: 12
        })
        const projectCategoryData = await projectCategoryCoreService.getList({
            filter: {
                status: "active"
            },
            fields: ["$all"],
            limit: 1000
        })

        console.log("pros: ", projectCategoryData)
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views`), { pageData, projectData, projectCategoryData, isMobile, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderBlog(req: Request, res: Response) {
        const blogData = await blogCoreService.getList({
            filter: {
                status: "active"
            },
            fields: ["$all"],
            limit: 1000
        })
        const postData = await postCoreService.getList({
            filter: {
                status: "active"
            },
            fields: ["$all"],
            limit: 10,
            offset: (req.query.page - 1) * 10 || 0
        })
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "blog"
            },
            fields: ["title", "metaTitle", "metaDescription", "metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/blog`), { pageData, blogData, postData, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderPostCategory(req: Request, res: Response) {
        const post = await postCoreService.getItemBySlug(req.params.slug, {
            fields: ["$all"]
        })
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "post_category"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/category`), { pageData, post, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderPost(req: Request, res: Response) {
        try {
            const post = await postCoreService.getItemBySlug(req.params.slug, {
                filter: {
                    status: "active"
                },
                fields: ["$all"]
            })
            if (!post.hasOwnProperty("_id")) {
                throw new Error()
            }
            let pageData = {}
            pageData = await pageCoreService.find({
                filter: {
                    appId: config.app._id,
                    link: "post"
                },
                fields: ["metaData"]
            }).then(result => {
                return result
            }).catch(err => {
                return {}
            })
            const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
            res.render(path.join(__dirname, `../../views/post`), { pageData, post, setting }, function (error, html) {
                if (error) {
                    res.redirect("/404")
                } else {
                    res.send(html)
                }
            })
        } catch (err) {
            res.redirect("/404")
        }
    }

    async renderContact(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "contact"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/contact`), { pageData, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async createContact(req: Request, res: Response) {
        const body = {
            fullName: req.body.fullName,
            email: req.body.email,
            body: req.body.body,
            appId: config.app._id,
            phone: req.body.phone
        }
        const result = await contactCoreService.create(body)
        res.status(200).json(result)
    }

    async renderAbout(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "about"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/about`), { pageData, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderServices(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "service"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const services = await serviceCoreService.getList({
            fields: ["$all"]
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/services`), { pageData, services, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async renderServiceDetail(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "service_detail"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const service = await serviceCoreService.getItemBySlug(req.params.slug, {
            fields: ["$all"]
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/service`), { pageData, service, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async renderProjects(req: Request, res: Response) {
        let isMobile: boolean = false
        const md = new MobileDetect(req.headers["user-agent"]);
        if (md.os() === "AndroidOS" || md.os() === "iOS") {
            isMobile = true
        }
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "service"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const projectData = await projectCoreService.getList({
            filter: {
                status: "active"
            },
            fields: ["$all"],
            limit: 10,
            offset: (req.query.page - 1) * 10 || 0
        })
        const projectCategoryData = await projectCategoryCoreService.getList({
            filter: {
                status: "active"
            },
            fields: ["$all"],
            limit: 1000
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/projects`), { pageData, projectCategoryData, projectData, setting, isMobile }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async renderProjectDetail(req: Request, res: Response) {
        try {
            let pageData = {}
            pageData = await pageCoreService.find({
                filter: {
                    appId: config.app._id,
                    link: "service_detail"
                },
                fields: ["metaData"]
            }).then(result => {
                return result
            }).catch(err => {
                return {}
            })
            const project = await projectCoreService.getItemBySlug(req.params.slug, {
                filter: {
                    status: "active"
                },
                fields: ["$all"]
            })
            if (!project.hasOwnProperty("_id")) {
                throw new Error()
            }
            const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
            res.render(path.join(__dirname, `../../views/project`), { pageData, project, setting }, function (error, html) {
                if (error) {
                    res.redirect("/404")
                } else {
                    res.send(html)
                }
            })
        } catch (err) {
            res.redirect("/404")
        }
    }

    async renderEvents(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "event"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/events`), { pageData, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async renderEventsDetail(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "event_detail"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/eventDetail`), { pageData, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async renderTeam(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "team"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const teams = await teamCoreService.getList({
            fields: ["$all"]
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/team`), { pageData, teams, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderTeamDetail(req: Request, res: Response) {
        let pageData = {}
        pageData = await pageCoreService.find({
            filter: {
                appId: config.app._id,
                link: "team_detail"
            },
            fields: ["metaData"]
        }).then(result => {
            return result
        }).catch(err => {
            return {}
        })
        const team = await teamCoreService.getItemBySlug(req.params.slug, {
            fields: ["$all"]
        })
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/teamDetail`), { pageData, team, setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async renderTimeline(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/timeline`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }
    async renderError(req: Request, res: Response) {
        const setting = require(path.join(__dirname, `../../views/setting/setting.json`))
        res.render(path.join(__dirname, `../../views/404`), { setting }, function (error, html) {
            if (error) {
                res.redirect("/404")
            } else {
                res.send(html)
            }
        })
    }

    async createComment(req: Request, res: Response) {
        res.status(200).json(req.body)
    }

}

