import * as express from 'express'
import * as cors from 'cors'
import * as mongoose from 'mongoose'
import * as morgan from 'morgan'
import * as bodyParser from 'body-parser'
import * as path from 'path'
import { config } from './config'

import { view, api } from './routers'


class Server {
    constructor() {
        this.port = process.env.PORT || 5000;
        this.server = express();
        // this.initDB();
        this.initMiddlewares();
        this.initView();
        this.initRoute();
        this.initStatisFolder();
        this.initServer();
    }
    port: any
    server: any

    async initDB() {
        mongoose.connect(config.database.mongo, {
            useNewUrlParser: true,
            useFindAndModify: false
        });
    }

    async initMiddlewares() {
        this.server.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
        this.server.use(bodyParser.urlencoded({ extended: false }));
        this.server.use(bodyParser.json());
        this.server.use(cors())

    }
    async initRoute() {
        this.server.use('/api', api)
    }

    async initStatisFolder() {
        this.server.set('view engine', 'ejs');
        //this.server.use(express.static(__dirname + `../views`));
        this.server.use(express.static('views'));
    }
    async initView() {
        this.server.use(view)
    }


    async initServer() {
        this.server.locals.moment = require('moment');
        this.server.listen(this.port, (err) => {
            console.log('Server listening on port ', this.port);
        });
    }
}

const server = new Server();
